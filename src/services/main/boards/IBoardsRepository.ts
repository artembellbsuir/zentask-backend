import { Board } from "../../../domain/Board";

export interface IBoardsRepository {
    create(title: string): boolean;
    getById(id: string): Board;
    arhive(): boolean;
}

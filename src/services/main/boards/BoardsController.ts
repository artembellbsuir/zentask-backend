import { Container, injectable, inject } from "inversify";
import { BoardsService } from "../../../domain/BoardsService";
import { IController } from "./IController";

@injectable()
export class BoardsController implements IController {
    constructor(private readonly boards: BoardsService) {}
    register(server: any): void {
        throw new Error("Method not implemented.");
    }
}

// server.get("/boards", async (request, reply) => {
//     return this.boards.getAllBoards();
// });

export class Board {
    id: string;
    projectId: string;
    title: string;
    createdDate: Date;
    archived: boolean;

    constructor(title: string, createdDate: Date, archived: boolean = false) {
        this.id = "";
        this.projectId = "";
        this.title = title;
        this.createdDate = createdDate;
        this.archived = archived;
    }
}

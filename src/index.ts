import fastify from "fastify";
import { container } from "./CompositionRoot";
import { BoardsController } from "./services/main/boards/BoardsController";
import { IController } from "./services/main/boards/IController";

const server = fastify({});

// register controllers

const controllers = container.getAll<IController>();
for (const controller of controllers) {
    controller.register(server);
}

server.listen(8080, (err, address) => {
    if (err) {
        console.error(err);
        process.exit(1);
    }
    console.log(`Server listening at ${address}`);
});

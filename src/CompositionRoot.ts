import { Container, injectable, inject } from "inversify";
import { BoardsService } from "./domain/BoardsService";
import { BoardsController } from "./services/main/boards/BoardsController";
import { IController } from "./services/main/boards/IController";

export const container = new Container();
container.bind<BoardsService>(BoardsService).toSelf();
container.bind<IController>(BoardsController).to(BoardsController);

// export const Init = () => {
//     // bind

// }
